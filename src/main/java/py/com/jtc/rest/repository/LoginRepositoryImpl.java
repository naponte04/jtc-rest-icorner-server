package py.com.jtc.rest.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.User;
@Repository
public class LoginRepositoryImpl implements LoginRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public User obtenerUsuario(String nrodocumento) {
		User user = jdbcTemplate.queryForObject("select * from usuarios b where  b.nrodocumento = ?",
				new Object[] {nrodocumento}, 
				(rs, rowNum) -> {
					User u = new User();
					u.setIdUsuario(rs.getInt("idusuario"));
					u.setNombre(rs.getString("nombre"));
					u.setPin(rs.getString("pin"));
					return u;
		        });
		
		   return user;
	}


}