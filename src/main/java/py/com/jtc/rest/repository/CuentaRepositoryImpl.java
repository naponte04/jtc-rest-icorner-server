package py.com.jtc.rest.repository;


import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Cuenta;

@Repository
public class CuentaRepositoryImpl implements CuentaRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Cuenta> obtenerCuentas(String nroDocumento) {
		List<Cuenta> cuentas = jdbcTemplate.query("select a.* from cuentas a, usuarios_cuentas b, usuarios c where a.idcuenta = b.idcuenta and b.idusuario=c.idusuario and c.nrodocumento = ?",
				new Object[] {nroDocumento},
				(rs) -> {
					List<Cuenta> list = new ArrayList<>();
					while(rs.next()){
						Cuenta c = new Cuenta();
						c.setIdCuenta(rs.getInt("idcuenta"));
						c.setNroCuenta(rs.getString("nrocuenta"));
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return cuentas;
	}
	
	@Override
	public Cuenta obtenerEmpleado(Integer id) {
		Cuenta cuentas = jdbcTemplate.queryForObject("select * from cuentas where idcuenta = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Cuenta c = new Cuenta();
					c.setIdCuenta(rs.getInt("idcuenta"));
					c.setNroCuenta(rs.getString("nrocuenta"));
					c.setTipo(rs.getString("tipo"));
					c.setMoneda(rs.getString("moneda"));
					c.setSaldo(rs.getInt("saldo"));
					return c;
		        });
		
		return cuentas;
	}

	@Override
	public Cuenta agregarEmpleado(Cuenta cuenta) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into cuentas values (?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setString(2, cuenta.getNroCuenta());
			ps.setString(3,  cuenta.getTipo());
			ps.setString(3,  cuenta.getMoneda());
			ps.setInt(4, cuenta.getSaldo());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			cuenta.setIdCuenta(keyHolder.getKey().intValue());
		
		return cuenta;
		
	}

	/*@Override
	public boolean eliminarEmpleado(Integer id) {
		int result = jdbcTemplate.update("delete from empleados where id = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarEmpleado(Cuenta empleado, Integer id) {
		int result = jdbcTemplate.update("update empleados set nombre = ?, edad = ?, salario = ? where id = ?", 
				empleado.getNombre(), empleado.getEdad(), empleado.getSalario(), id);
		return (result > 0) ? true : false;
	}*/

}
