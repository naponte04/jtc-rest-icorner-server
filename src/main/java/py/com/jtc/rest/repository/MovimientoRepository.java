package py.com.jtc.rest.repository;

import java.util.List;

import py.com.jtc.rest.bean.Movimiento;

public interface MovimientoRepository {
	List<Movimiento> obtenerMovimiento(String nroDocumento);
}
