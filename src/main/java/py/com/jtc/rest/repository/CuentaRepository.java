package py.com.jtc.rest.repository;

import java.util.List;

import py.com.jtc.rest.bean.Cuenta;

public interface CuentaRepository {

	List<Cuenta> obtenerCuentas(String nroDocumento);
	
	Cuenta obtenerEmpleado(Integer idcuenta);
	
	Cuenta agregarEmpleado(Cuenta cuenta);
	
	/*boolean eliminarEmpleado(Integer id);
	
	boolean actualizarEmpleado(Cuenta contacto, Integer id);*/
}
