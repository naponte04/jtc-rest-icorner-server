package py.com.jtc.rest.repository;

import py.com.jtc.rest.bean.User;

public interface LoginRepository {
	
	User obtenerUsuario(String nrodocumento);
}
