package py.com.jtc.rest.repository;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Movimiento;

@Repository
public class MovimientoRepositoryImpl implements MovimientoRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Movimiento> obtenerMovimiento(String nroDocumento) {
		List<Movimiento> movimiento = jdbcTemplate.query("select g.* from movimientos g, cuentas a, usuarios_cuentas b, usuarios c where a.idcuenta = b.idcuenta and b.idusuario=c.idusuario and g.idcuenta=b.idcuenta and a.idcuenta=g.idcuenta and c.nrodocumento = ?",
				new Object[] {nroDocumento},
				(rs) -> {
					List<Movimiento> list = new ArrayList<>();
					while(rs.next()){
						Movimiento c = new Movimiento();
						c.setIdMovimiento(rs.getInt("idmovimiento"));
						c.setFechaHora(rs.getDate("fechahora"));
						c.setIdCuenta(rs.getInt("idcuenta"));
						c.setMonto(rs.getInt("monto"));
						c.setTipoMovimiento(rs.getString("tipomovimiento"));
						list.add(c);
					}
					return list;
				});
		return movimiento;
	}
	
	
}
