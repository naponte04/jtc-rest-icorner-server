package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.Cuenta;
import py.com.jtc.rest.repository.CuentaRepository;


@RestController
@RequestMapping("/rest-client/cuentas")
public class CuentaController {

	@Autowired
	private CuentaRepository cuentaRepository;
	
	@GetMapping("/{nroDocumento}")
	public List<Cuenta> obtenerCuentas(@PathVariable("nroDocumento") String nroDocumento){
        List<Cuenta> cuentas = cuentaRepository.obtenerCuentas(nroDocumento);
		
		if (cuentas.size() > 0) { 
			
		}
		return cuentas;
		
	}
	
//	@GetMapping("/{idcuenta}")
//	public Cuenta obtenerEmpleados(@PathVariable("idcuenta") String idcuenta) {
//       Cuenta cuenta = empleadoRepository.obtenerEmpleado(Integer.valueOf(idcuenta));
//		   return cuenta;
//			
//		
//	}
	
	@PostMapping
	public ResponseEntity<Cuenta> guardarEmpleado(@RequestBody Cuenta cuenta, UriComponentsBuilder uBuilder) {
		Cuenta c = cuentaRepository.agregarEmpleado(cuenta);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/empleados/" )
				.path(String.valueOf(cuenta.getIdCuenta()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(c, header, HttpStatus.CREATED);
	}
	
/*	@PutMapping("/{id}")
	public Cuenta editarEmpleado(@PathVariable("id") String id,
			@RequestBody Cuenta empleado, 
			UriComponentsBuilder uBuilder) 
	{
		Cuenta c = null;
		boolean result = empleadoRepository.actualizarEmpleado(empleado, Integer.valueOf(id));
		if (result) {
			 c = obtenerEmpleados(id);
		}
		return c;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarEmpleado(@PathVariable("id") String id) { 
		
		boolean delete = empleadoRepository.eliminarEmpleado(Integer.valueOf(id));
		return new ResponseEntity<Cuenta> (HttpStatus.NO_CONTENT);
	}*/
}
