package py.com.jtc.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import py.com.jtc.rest.bean.Cuenta;
import py.com.jtc.rest.bean.Movimiento;
import py.com.jtc.rest.repository.CuentaRepository;
import py.com.jtc.rest.repository.MovimientoRepository;

	@RestController
	@RequestMapping("/rest-client/movimientos")
	public class MovimientoController {

		@Autowired
		private MovimientoRepository movimientoRepository;
		
		@GetMapping("/{nroDocumento}")
		public List<Movimiento> obtenerMovimiento(@PathVariable("nroDocumento") String nroDocumento){
	        List<Movimiento> movimiento = movimientoRepository.obtenerMovimiento(nroDocumento);
			
			if (movimiento.size() > 0) { 
				
			}
			return movimiento;
			
		}
}
