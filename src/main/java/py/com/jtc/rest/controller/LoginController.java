package py.com.jtc.rest.controller;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import py.com.jtc.rest.bean.User;
import py.com.jtc.rest.repository.LoginRepository;


@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired 
	private LoginRepository loginRepository;
	
	@PostMapping("/{nrodocumento}")
	public boolean login(@RequestBody User user) {
		User usuario = loginRepository.obtenerUsuario(user.getNroDocumento());
		
		if (usuario != null) {
			
			if (usuario.getPin().equals(user.getPin())){
				System.out.println("login true");
				return true;
		    
			} 
			else
			{
				return false;
			}
		} 
		
		return false;
	}
}


